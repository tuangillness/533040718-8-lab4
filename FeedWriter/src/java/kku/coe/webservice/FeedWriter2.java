/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import javax.xml.stream.*;

/**
 *
 * @author Nopsompong
 */
@WebServlet(name = "FeedWriter2", urlPatterns = {"/FeedWriter2"})
public class FeedWriter2 extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text / html;charset = UTF -­8");
        PrintWriter out = response.getWriter();
        try{
new
FeedWriter2().createRssDoc();
out.println("<b><a href='http://localhost:8080/FeedWriter/feed2.xml'>" + "Rss Feed</a> was create successfully</b>");
}catch(Exception e){
System.out.println(e);
}
        
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    }
    public void createRssDoc()
            throws Exception {
        String filename = "C:/Users/Puch_zaa/Documents/NetBeansProjects/FeedWriter/web/feed2.xml";
        File file = new File(filename);
        XMLOutputFactory xof =
                XMLOutputFactory.newInstance();
        XMLStreamWriter xtw =
                xof.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file)));
        xtw.writeStartDocument();
        xtw.writeStartElement("rss");
        xtw.writeAttribute("version", "2.0");
        xtw.writeStartElement("channel");
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean University RSS Feed");
        xtw.writeEndElement();
        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean University Information News RSS Feed");
        xtw.writeEndElement();
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();
        xtw.writeStartElement("lang");
        xtw.writeCharacters("en~th");
        xtw.writeEndElement();
        xtw.writeStartElement("item");
        xtw.writeStartElement("title");
        xtw.writeCharacters("นักศึกษามข.คว้ารางวัลโปสเตอร์ยอดเยี่ยมในการประชุมวิชาการนานาชาติ");
        xtw.writeEndElement();
        xtw.writeStartElement("description");
        xtw.writeCharacters("นักศึกษาปริญญาเอกของภาควิชาชีววิทยาคณะวิทยาศาสตร์" + "คว่ารางวัลโปสเตอร์ยอดเยี่ยมในการประชุมวิชาการนานาชาติ ABIC 2009");
        xtw.writeEndElement();
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.news.kku.ac.th/kkunews/index.php?option=com_content&task=view&id=3091&Itemid=48");
        xtw.writeEndElement();
        xtw.writeStartElement("pubDate");
        xtw.writeCharacters((new java.util.Date()).toString());
        xtw.writeEndElement();
        xtw.writeEndElement();//end element item 
        xtw.writeEndElement();//endelement channel 
        xtw.writeEndElement();//end element rss
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
