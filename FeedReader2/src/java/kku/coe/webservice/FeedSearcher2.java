package kku.coe.webservice;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.net.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author Nopsompong
 */
@WebServlet(name = "FeedSearcher2", urlPatterns = {"/FeedSearcher2"})
public class FeedSearcher2 extends HttpServlet {

    XMLEventReader reader;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8"); 
        PrintWriter out = response.getWriter();
        
        String input = request.getParameter("url");
        String keyloc = request.getParameter("keyword");
        
        boolean titleFound = false;
        boolean linkFound = false;
        boolean itemFound = false;
        boolean check1 = false;
        
        String link = null;
        String eName;
        try {
            URL u = new URL(input);
            InputStream in = u.openStream();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            reader = factory.createXMLEventReader(in);
            out.print("<html><body><table border = '1'><tr><th>Title</th><th>Link</th></tr>");
            
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = true;
                    }
                    if (itemFound && eName.equals("link")) {
                        linkFound = true;
                    }
                    if (itemFound && eName.equals("title")) {
                        titleFound = true;
                     }
                }
                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = false;
                    }
                    if (itemFound && eName.equals("link")) {
               
                        linkFound = false;
                    }
                    if (itemFound && eName.equals("title")) {
                        
                        titleFound = false;
                    }
                }
                if (event.isCharacters()) {
                    Characters characters = (Characters) event;
                    if (linkFound) {
                         if (check1) {
                            out.print("<td>");
                            out.print(characters.getData());
                            out.print("</td></tr>");
                            check1 = false;
                        }
                     
                    }
                    if (titleFound) {
                       if (characters.getData().toLowerCase().contains(keyloc.toLowerCase())) {
                            check1 = true;
                            out.print("<tr><td>");
                            out.print(characters.getData());
                            out.print("</td>");
                    }
                 }
            }
            }
            reader.close();
            out.print("</table></body></html>");
            } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
