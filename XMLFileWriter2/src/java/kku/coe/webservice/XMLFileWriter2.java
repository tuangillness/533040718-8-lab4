/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;

import java.io.FileWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author Nopsompong
 */
public class XMLFileWriter2 {

    public static void main(String args[]) {
        try {
            String fileName = "quote.xml";
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw = xof.createXMLStreamWriter(new FileWriter(fileName));

            xtw.writeStartDocument("UTF-8", "1.0");
            xtw.writeStartElement("quotes");
            xtw.writeStartElement("quote");

            xtw.writeStartElement("word");
            xtw.writeCharacters("Time is more value than money. You can get more money,but you cannot get more time");
            xtw.writeEndElement();

            xtw.writeStartElement("by");
            xtw.writeCharacters("Jim Rohn");
            xtw.writeEndElement();

            xtw.writeEndElement();

            xtw.writeStartElement("quote");

            xtw.writeStartElement("word");
            xtw.writeCharacters("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            xtw.writeEndElement();

            xtw.writeStartElement("by");
            xtw.writeCharacters("ว.วชิรเมธี");
            xtw.writeEndElement();

            xtw.writeEndElement();

            xtw.writeEndElement();

            xtw.writeEndDocument();

            xtw.flush();
            xtw.close();


        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
