/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;


import java.io.IOException;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import org.w3c.dom.*;
import javax.servlet.http.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author Nopsompong
 */
@WebServlet(name = "XMLFileWriter", urlPatterns = {"/XMLFileWriter"})
public class XMLFileWriter {

    public static void main(String args[]) {

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder parser = dbf.newDocumentBuilder();
            Document doc = parser.newDocument();

            Element quotes = doc.createElement("quotes");
            doc.appendChild(quotes);

            Element quote = doc.createElement("quote");
            quotes.appendChild(quote);

            Element word = doc.createElement("word");
            word.appendChild(doc.createTextNode("Time is more value than money. You can get more money,but you cannot get more time"));
            quote.appendChild(word);

            Element by = doc.createElement("by");
            by.appendChild(doc.createTextNode("Jim Rohn"));
            quote.appendChild(by);

            Element quote1 = doc.createElement("quote");
            quotes.appendChild(quote1);

            Element word1 = doc.createElement("word");
            word1.appendChild(doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง"));
            quote1.appendChild(word1);

            Element by1 = doc.createElement("by");
            by1.appendChild(doc.createTextNode("ว.วชิรเมธี"));
            quote1.appendChild(by1);

            Source source = new DOMSource(doc);
            File file = new File("quote.xml");
            Result result = new StreamResult(file);
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(OutputKeys.INDENT, "yes");
            xformer.transform(source, result);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
