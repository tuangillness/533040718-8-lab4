/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;

import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 *
 * @author Nopsompong
 */
/**
 *
 * @param parent
 * @param lable
 * @return
 */
import java.io.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import javax.xml.stream.*;

public class XMLFileSearcher {

    public static void main(String argv[]) throws FileNotFoundException, UnsupportedEncodingException, ParserConfigurationException, SAXException, IOException {
        String qoutes = "C:\\qoutes.xml";
        String keyword = "C:\\keyword.txt";
        File keyfile = new File(keyword);
        File filename = new File(qoutes);
        String word = "";
        BufferedReader key = null;
        key = new BufferedReader(new InputStreamReader(
                new FileInputStream(keyfile), "UTF-8"));


        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filename);
            while ((word = key.readLine().toLowerCase()) != null) {

                NodeList quote = doc.getElementsByTagName("quote");
                for (int i = 0; i < quote.getLength(); i++) {
                    Element quoteI = (Element) quote.item(i);
                    String by = XMLFileSearcher.getElemVal(quoteI, "by");
                    String words = XMLFileSearcher.getElemVal(quoteI, "words");

                    if (by.toLowerCase().indexOf(word) != -1) {
                        System.out.println(getElemVal(quoteI, "word") + " by " + getElemVal(quoteI, "by"));
                    }
                }

            }
        } finally {
            key.close();
            System.exit(0);

        }
    }

    protected static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
}
